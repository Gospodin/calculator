package fox1q;

public class Calculator {

    /**
     * Метод сложения двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает результат сложения
     **/
    public static double addiction(double operand1, double operand2) {
        return operand1 + operand2;
    }

    /**
     * Метод вычитания двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает результат вычитания
     **/
    public static double subtraction(double operand1, double operand2) {
        return operand1 - operand2;
    }

    /**
     * Метод умножения двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает результат умножения
     **/
    public static double multiplication(double operand1, double operand2) {
        return operand1 * operand2;
    }

    /**
     * Метод вещественного деления двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает результат вещественного деления
     **/
    public static double division(double operand1, double operand2) {
        return operand1 / operand2;
    }

    /**
     * Метод целочисленного деления двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает результат целочисленного деления
     **/
    public static int division(int operand1, int operand2) {
        return operand1 / operand2;
    }

    /**
     * Метод деления двух операндов с выделением остатка
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает остаток
     **/
    public static double division2(double operand1, double operand2) {
        return operand1 % operand2;
    }

    /**
     * Метод возведения в степень двух операндов
     * @param operand1 - первый операнд
     * @param operand2 - второй операнд
     * @return - возвращает возведенное в степень число
     **/
    public static double pow(double operand1, double operand2) {
        double result = 1;
        if (operand2 == 0) {
            result = 1;
        }
        if (operand2 == 1){
            result = operand1;
        }
        if (operand2 == -1){
            result = 1 / operand1;
        }
        if(operand2 > 1) {
            for (int i = 0; i < operand2; i++){
                result *= operand1;
            }
        }
        if(operand2 < -1) {
            for (int i = 0; i > operand2; i--){
                result *= operand1;
            }
            result = 1 / result;
        }
        return result;
    }
}