package fox1q;

import java.util.Scanner;

public class Demo {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.println("Введите первый операнд:");
        double operand1 = in.nextDouble();
        System.out.println("Введите второй операнд:");
        double operand2 = in.nextDouble();
        System.out.println("Сложение: " + Calculator.addiction(operand1, operand2));
        System.out.println("Вычитание: " + Calculator.subtraction(operand1, operand2));
        System.out.println("Умножение: " + Calculator.multiplication(operand1, operand2));
        System.out.println("Деление: " + Calculator.division(operand1, operand2));
        System.out.println("Деление с выделением остатка: " + Calculator.division2(operand1, operand2));
        System.out.println("Возведение в степень: " + Calculator.pow(operand1, operand2));
    }
}